#include <iostream>
#include <fstream>
#include <chrono>

#include <SFML/Graphics.hpp>

#define TIMER_RUNNING_COLOR sf::Color::Green
#define TIMER_STOPPED_COLOR sf::Color(0xddddddff)
#define BACKGROUND_COLOR sf::Color::Black

std::string addZeros(unsigned long time, unsigned int minChars)
{
    std::string result = std::to_string(time);

    for (int i = std::to_string(time).length(); i < minChars; i++)
    {
        result = "0" + result;
    }

    return result;
}

std::string timeToString(unsigned long time)
{
    std::string result;
    if (time / 3600000 == 0) // Hide hours
    {
        result = addZeros(time / 60000 % 60, 2) + ":" + addZeros(time / 1000 % 60, 2) + "." + addZeros(time % 1000, 3);
    }
    else // Display hours
    {
        result = addZeros(time / 3600000, 1) + ":" + addZeros(time / 60000 % 60, 2) + ":" + addZeros(time / 1000 % 60, 2) + "." + addZeros(time % 1000, 3);
    }
    
    return result;
}

int main(int argc, char* argv[])
{
    std::string filename = "";
    std::string displayFilename = "No file opened";
    if (argc > 1)
    {
        filename = std::string(argv[1]);
        std::cout << "Loading from file" << filename << "...\n";
        displayFilename = filename;
    }
    
    sf::RenderWindow window(sf::VideoMode(500, 100), displayFilename + " - Timer by TadeLn");
    sf::Font font;
    font.loadFromFile("Arial.ttf");

    sf::Text timerText;
    timerText.setFont(font);
    timerText.setCharacterSize(50.f);
    timerText.setFillColor(TIMER_STOPPED_COLOR);
    timerText.setPosition(0, 0);

    std::chrono::steady_clock::time_point startTimePoint = std::chrono::steady_clock::now();

    bool timerRunning = false;
    unsigned long long int savedTime = 0;
    if (filename != "")
    {
        std::string line;
        std::ifstream file(filename);
        if (file.is_open())
        {
            std::getline(file, line);
            if (line != "")
            {
                savedTime = std::stoll(line);
            }
            else
            {
                savedTime = 0;
            }
            
            file.close();
        }
        else
        {
            std::cout << "Error while opening file " << filename << "\n";
        }
        
    }
    unsigned long long int currentTime = savedTime;

    timerText.setString(timeToString(currentTime));
    bool pressedPauseOnCurrentFrame = false;
    bool pressedPauseOnPreviousFrame = false;
    while (window.isOpen())
    {
        currentTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - startTimePoint).count();

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
            if (event.type == sf::Event::Resized)
            {
                sf::FloatRect visibleArea(0.f, 0.f, event.size.width, event.size.height);
                window.setView(sf::View(visibleArea));
            }
        }

        // Detect keypresses
        pressedPauseOnCurrentFrame = sf::Keyboard::isKeyPressed(sf::Keyboard::Pause);
        if (pressedPauseOnCurrentFrame && !pressedPauseOnPreviousFrame)
        {
            timerRunning = !timerRunning;
            if (timerRunning)
            {
                startTimePoint = std::chrono::steady_clock::time_point(std::chrono::steady_clock::now() - std::chrono::milliseconds(savedTime));
                timerText.setFillColor(TIMER_RUNNING_COLOR);
            }
            else
            {
                timerText.setFillColor(TIMER_STOPPED_COLOR);
                savedTime = currentTime;
                
                std::ofstream file;
                if (filename.compare(""))
                {
                    file.open(filename, std::ios::trunc);
                }
                else
                {
                    file.open("temp.ttimer", std::ios::trunc);
                }
                
                if (file.is_open())
                {
                    file << std::to_string(savedTime);
                    file.close();
                }
                else
                {
                    std::cout << "Error while opening file " << filename << "\n";
                }
            }
        }
        pressedPauseOnPreviousFrame = pressedPauseOnCurrentFrame;
        

        if (timerRunning)
        {
            timerText.setString(timeToString(currentTime));
        }

        window.clear(BACKGROUND_COLOR);
        window.draw(timerText);
        window.display();
    }

    return 0;
}